export const replaceString = (string, arg1, arg2) => {
  return string?.replace(arg1 ?? "/", arg2 ?? "");
};

const test = replaceString("he//llo");
console.log(test);
