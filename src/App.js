import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { useState, useEffect } from "react";
import { Routes, Route, useLocation } from "react-router-dom";
import Home from "./Pages/Home/Home";
import About from "./Pages/About";
import Services from "./Pages/Services";
import Contact from "./Pages/Contact";
import Team from "./Pages/Team";
import Partner from "./Pages/Partner";
import Header from "./Component/Header";
import Footer from "./Component/Footer";
import EachPageHeader from "./Component/EachPageHeader";
import useGetPathName from "./Hooks/useGetPathName";

function App() {
  // const location = useLocation().pathname;
  // let pathName = replaceString(location);

  const pathName = useGetPathName();

  //active menu link
  const [isActive, setIsActive] = useState();

  useEffect(() => {
    window.scrollTo(0, 0);

    setIsActive(pathName);
  }, [pathName]);

  return (
    <div className="App">
      <Header isActive={isActive} />
      {pathName === "" ? "" : <EachPageHeader />}
      <Routes>
        <Route path={"/"} element={<Home />}></Route>
        <Route path="/about" element={<About />}></Route>
        <Route path="/ourservices" element={<Services />}></Route>
        <Route path="/ourteam" element={<Team />}></Route>
        <Route path="/partner" element={<Partner />}></Route>
        <Route path="/contactus" element={<Contact />}></Route>
        <Route path="*" element={<h1>404</h1>}></Route>
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
