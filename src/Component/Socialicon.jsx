import React from "react";
import { Nav } from "react-bootstrap";
import { socialIcon } from "./../constants/socialicon";

function Socialicon() {
  return (
    <div>
      <Nav className="justify-content-md-end">
        {socialIcon.map((item, index) => {
          return (
            <Nav.Item key={index}>
              <Nav.Link href="#" className="px-1">
                <span className="social_link fs-3">{item}</span>
              </Nav.Link>
            </Nav.Item>
          );
        })}
      </Nav>
    </div>
  );
}

export default Socialicon;
