import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "react-bootstrap/esm/";
import { Link } from "react-router-dom";
import { getAllMenu } from "../api";
import { contactItem } from "./../constants/contact";

import Socialicon from "./Socialicon";

function Footer() {
  const [menuLink, setMenuLink] = useState([]);
  useEffect(() => {
    response();
  }, []);

  async function response() {
    try {
      const res = await getAllMenu();
      setMenuLink(res);
    } catch (error) {
      console.log(error);
    }
  }

  return (
    <div className="h-100 bg-dark text-light">
      <Container fluid>
        <Container className="pt-5 px-0">
          <Row>
            <Col xl={3} lg={3} md={3} className="mb-5">
              Logo Company
            </Col>
            <Col xl={3} lg={3} md={3} className="mb-4">
              <h5 className="text-light mb-4">Link</h5>
              <ul className="list-unstyled">
                {menuLink.map((item, index) => {
                  if (item.path === "") return;
                  return (
                    <Link
                      to={`/${item.path}`}
                      key={index}
                      className="text-decoration-none d-block pb-2 "
                    >
                      <span className="link-footer">{item.menu}</span>
                    </Link>
                  );
                })}
              </ul>
            </Col>
            <Col xl={3} lg={3} md={3} className="mb-4">
              <h5 className="text-light mb-4">Contact Us</h5>
              {contactItem.map((item, index) => {
                if (index === 2) return;
                return (
                  <div key={index}>
                    <p className="mb-2">{item.label}</p>
                    <p className="text-white-50">{item.value}</p>
                  </div>
                );
              })}
            </Col>

            <Col xl={3} lg={3} md={3}>
              <h5 className="text-light mb-4">Address</h5>
              {contactItem.map((item, index) => {
                if (index === 2)
                  return (
                    <p key={index} className="text-white-50">
                      {item.value}
                    </p>
                  );
              })}
            </Col>
          </Row>
        </Container>

        <Container className="border-top border-light pb-4 px-0">
          <Row>
            <Col>
              <p className="mt-4 text-white-50">
                © 2022 Company, Inc. All rights reserved
              </p>
            </Col>
            <Col xl={6} lg={6} md={6} className="mt-3">
              <Socialicon />
            </Col>
          </Row>
        </Container>
      </Container>
    </div>
  );
}

export default Footer;
