import React from "react";
import { Link, useLocation } from "react-router-dom";
import { Container, Breadcrumb } from "react-bootstrap/esm/";
import img12 from "../Images/12.jpg";
import { menuLink } from "./../constants/index";
import useGetPathName from "./../Hooks/useGetPathName";

function EachPageHeader() {
  let pathName = useGetPathName();

  const titleHeader = menuLink.find((x) => x.path === pathName).title;

  return (
    <div>
      <Container
        fluid
        className="bg-about"
        style={{ backgroundImage: `url(${img12})` }}
      >
        <div className="bg">
          <Container className="px-0">
            <h1 className="pt-5 pb-2 text-white">
              <strong>{titleHeader}</strong>
            </h1>
            <Breadcrumb>
              <Breadcrumb.Item>
                <Link to="/">Home</Link>
              </Breadcrumb.Item>
              <Breadcrumb.Item active>{titleHeader}</Breadcrumb.Item>
            </Breadcrumb>
          </Container>
        </div>
      </Container>
    </div>
  );
}

export default EachPageHeader;
