import { React, useState, useRef, useEffect } from "react";
import { Nav, Navbar, Offcanvas } from "react-bootstrap";
import { Link } from "react-router-dom";
import { BsSearch } from "react-icons/bs";
import { BiTime } from "react-icons/bi";
import { contactItem } from "./../constants/contact";
import Socialicon from "./Socialicon";
import { getAllMenu } from "../api";
import {
  Container,
  Row,
  Col,
  Popover,
  Overlay,
  Form,
} from "react-bootstrap/esm";

function Header(props) {
  //Search
  const [show, setShow] = useState(false);
  const [target, setTarget] = useState(null);
  const [menuLink, setMenuLink] = useState([]);

  const ref = useRef(null);
  const handleSearchClick = (event) => {
    setShow(!show);
    setTarget(event.target);
  };

  //sticky
  const [scrolled, setScrolled] = useState(false);
  const handleScroll = () => {
    const offset = window.scrollY;
    if (offset > 40) {
      setScrolled(true);
    } else {
      setScrolled(false);
    }
  };
  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    response();
  }, []);

  async function response() {
    try {
      const res = await getAllMenu();
      setMenuLink(res);
    } catch (error) {
      console.log(error);
    }
  }

  return (
    <div>
      <Container fluid className="top-header text-light">
        <Container className="px-0">
          <Row>
            <Col xl={6} lg={6} md={6} className="pt-2">
              {contactItem.map((item, index) => {
                if (item.key === 4)
                  return (
                    <div key={index}>
                      <BiTime />
                      &nbsp;<span>{item.label}</span>: <span>{item.value}</span>
                    </div>
                  );
              })}
            </Col>
            <Col xl={6} lg={6} md={6} className="d-flex justify-content-md-end">
              <span className="pt-2">Follow Us On:</span>
              <span className="text-info">
                <Socialicon />
              </span>
            </Col>
          </Row>
        </Container>
      </Container>
      <Container
        fluid
        className="bg-light"
        style={{
          position: scrolled ? "fixed" : "",
          top: scrolled ? 0 : "",
          boxShadow: "1px 1px 2px 1px #ccc",
          zIndex: 2000,
          transition: "right .5s ease;",
        }}
      >
        <Container className="px-0">
          <Navbar key="lg" bg="light" expand="lg">
            <Navbar.Brand href="#">Company Logo</Navbar.Brand>
            <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-lg`} />
            <Navbar.Offcanvas
              id={`offcanvasNavbar-expand-lg`}
              aria-labelledby={`offcanvasNavbarLabel-expand-lg`}
              placement="end"
              scroll="true"
              backdrop="static"
            >
              <Offcanvas.Header closeButton>
                <Offcanvas.Title id={`offcanvasNavbarLabel-expand-lg`}>
                  Company Logo
                </Offcanvas.Title>
              </Offcanvas.Header>
              <Offcanvas.Body>
                <Nav className="justify-content-center flex-grow-1 fs-5 pe-3">
                  {console.log("menuLink", menuLink)}
                  {menuLink.map((item, index) => {
                    return (
                      <Nav.Link style={{ padding: "10px 20px" }} key={index}>
                        <Link
                          to={`/${item.path}`}
                          className="header-link text-decoration-none text-dark"
                          style={{
                            borderBottom:
                              props?.isActive === item.path
                                ? "3px solid #08B8F8"
                                : "",
                          }}
                        >
                          <strong>{item.menu}</strong>
                        </Link>
                      </Nav.Link>
                    );
                  })}
                </Nav>
              </Offcanvas.Body>
            </Navbar.Offcanvas>
            <div ref={ref}>
              <BsSearch onClick={handleSearchClick} />
              <Overlay
                show={show}
                target={target}
                placement="bottom"
                container={ref}
                containerPadding={20}
              >
                <Popover id="popover-contained">
                  <Form className="d-flex">
                    <Form.Control
                      className="border-0"
                      type="text"
                      placeholder="Type and hit enter..."
                      autoFocus="on"
                    ></Form.Control>
                  </Form>
                </Popover>
              </Overlay>
            </div>
          </Navbar>
        </Container>
      </Container>
    </div>
  );
}

export default Header;
