export const AddressMap = () => {
  return (
    <div className="google-map-code">
      <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d434.1439375324684!2d104.91627848842504!3d11.6323354502254!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbf5e856d01f5a52c!2z4Z6f4Z6f4Z6Z4Z6AIOGen-GfkuGek-GetuGegOGfi-GegOGetuGemuGel-GfkuGek-GfhuGeluGfgeGeiQ!5e0!3m2!1sen!2skh!4v1662112233091!5m2!1sen!2skh"
        width="100%"
        height="450"
        frameborder="0"
        style={{ border: 0 }}
        allowfullscreen=""
        aria-hidden="false"
        tabindex="0"
      ></iframe>
    </div>
  );
};
