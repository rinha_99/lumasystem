import axios from "axios";

// export const fetchAllServices = () => {
//   return axios
//     .get("http://localhost:3001/content/all")
//     .then((response) => {
//       return response.data;
//     })
//     .catch((error) => console.log(error));
// };

/**
 *
 * Asynchronus function
 *
 * inlcude async/await
 *
 */
export const getAllMenu = async () => {
  const response = await axios.get("http://localhost:3001/menu");
  return response.data;
};

export const getAllServices = async () => {
  const response = await axios.get("http://localhost:3001/content/all");
  return response.data;
};

export const getFrontServices = async () => {
  const response = await axios.get("http://localhost:3001/content");
  return response.data;
};
