import { HiOutlineMail, HiPhoneIncoming } from "react-icons/hi";

import { TbWorld } from "react-icons/tb";

export const contactItem = [
  {
    icon: <HiPhoneIncoming />,
    label: "Phone Number",
    value: "+855 23 727 262",
    key: 1,
  },
  {
    icon: <HiOutlineMail />,
    label: "Email",
    value: "info@stemcambodia.ngo",
    key: 2,
  },
  {
    icon: <TbWorld />,
    label: "Address",
    value:
      "Vichet Seulpak st. Preak Leap vaillage Sangkat Preak Leap, Phnom Penh",
    key: 3,
  },
  { label: "Business Hour", value: "Mon - Fri 8 AM - 5 PM", key: 4 },
];
