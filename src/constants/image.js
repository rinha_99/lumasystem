// export const img1 = require("../assets/images/carousel/1.jpeg");
// export const img2 = require("../assets/images/carousel/3.jpg");
// export const img3 = require("../assets/images/carousel/4.jpeg");

import img1 from "../assets/images/carousel/1.jpeg";
import img2 from "../assets/images/carousel/3.jpg";
import img3 from "../assets/images/carousel/4.jpeg";

import member5 from "../assets/images/ourteam/member5.jpg";
import member6 from "../assets/images/ourteam/member6.jpg";
import member7 from "../assets/images/ourteam/member7.jpg";
import member8 from "../assets/images/ourteam/member8.jpg";

import service1 from "../assets/images/servicecard/5.png";
import service2 from "../assets/images/servicecard/6.png";
import service3 from "../assets/images/servicecard/7.png";

import vision from "../assets/images/about/14.jpg";
import mission from "../assets/images/about/15.jpg";
import aboutus from "../assets/images/about/17.png";

export { img1, img2, img3 };
export { member5, member6, member7, member8 };
export { service1, service2, service3 };
export { vision, mission, aboutus };
