import { FaFacebook, FaTwitter, FaTelegram, FaInstagram } from "react-icons/fa";

export const socialIcon = [
  <FaFacebook />,
  <FaTwitter />,
  <FaTelegram />,
  <FaInstagram />,
];
