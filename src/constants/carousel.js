import { img1, img2, img3 } from "./image";

export const carouselItems = [
  {
    title: "First slide label",
    description: "Nulla vitae elit libero, a pharetra augue mollis interdum.",
    src: img1,
  },
  {
    title: "Second slide label",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    src: img2,
  },
  {
    title: "Third slide label",
    description:
      "Praesent commodo cursus magna, vel scelerisque nisl consectetur.",
    src: img3,
  },
];
