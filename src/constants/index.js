export const menuLink = [
  { title: "Home", path: "" },
  { title: "About", path: "about" },
  { title: "Our Services", path: "our_services" },
  { title: "Our Team", path: "our_team" },
  { title: "Partner", path: "partner" },
  { title: "Contact Us", path: "contact_us" },
];
