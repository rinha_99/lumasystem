import { service1, service2, service3 } from "./image";

export const cardServicesItem = [
  {
    src: service1,
    title: "IT Services",
    description:
      " We carry more than just good coding skills. Our experience makes us stand out from other web development.",
  },
  {
    src: service2,
    title: "Web Development",
    description:
      " We carry more than just good coding skills. Our experience makes us stand out from other web development.",
  },
  {
    src: service3,
    title: "Mobile App Development",
    description:
      " We carry more than just good coding skills. Our experience makes us stand out from other web development.",
  },
];
