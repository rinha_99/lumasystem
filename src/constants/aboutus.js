import { vision, mission, aboutus } from "./image";

export const aboutusItem = [
  {
    title: "About Us",
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Utdolorem placeat quo enim adipisci veniam molestias architectoaspernatur aut dolore itaque error eveniet a fuga, perferendissunt et! Aut, aperiam. Lorem ipsum dolor, sit amet consecteturadipisicing elit. Ut dolorem placeat quo enim adipisci veniammolestias architecto aspernatur aut dolore itaque error eveniet afuga, perferendis sunt et! Aut, aperiam",
    src: aboutus,
    key: 1,
  },
  {
    title: "Vision",
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Utdolorem placeat quo enim adipisci veniam molestias architectoaspernatur aut dolore itaque error eveniet a fuga, perferendissunt et! Aut, aperiam. Lorem ipsum dolor, sit amet consecteturadipisicing elit. Ut dolorem placeat quo enim adipisci veniammolestias architecto aspernatur aut dolore itaque error eveniet afuga, perferendis sunt et! Aut, aperiam",
    src: vision,
    key: 2,
  },
  {
    title: "Mission",
    description:
      "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Rerum,non rem? Nobis est cupiditate vero reprehenderit, ratione esse.Rerum et temporibus repellendus magni velit fuga cupiditate nisi,quae quis suscipit!Lorem ipsum dolor sit, amet consectetuadipisicing elit. Rerum, non rem? Nobis est cupiditate veroreprehenderit, ratione esse. Rerum et temporibus repellendus magvelit fuga cupiditate nisi, quae quis suscipit!",
    src: mission,
    key: 3,
  },
];
