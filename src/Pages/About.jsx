import React from "react";
import { Container, Row, Col } from "react-bootstrap/esm";
import { aboutusItem } from "./../constants/aboutus";

function About() {
  return (
    <div>
      <Container className="pt-5 pb-5 mt-4">
        <p className="text-info">// About us</p>
        <h1>
          <strong>What We Do</strong>
        </h1>
        <strong>
          {aboutusItem.map((item) => {
            if (item.key === 1)
              return (
                <p className="fs-5 text-secondary pt-3">{item.description}</p>
              );
          })}
        </strong>

        {aboutusItem.map((item) => {
          if (item.key === 2)
            return (
              <Row className="mt-5">
                <Col xl={7} lg={7} className="m-auto">
                  <h1 className="text-dark pb-3">{item.title}</h1>
                  <p className="text-secondary">{item.description}</p>
                </Col>
                <Col xl={5} lg={5}>
                  <img src={item.src} className="w-100 text-right" />
                </Col>
              </Row>
            );
        })}

        {aboutusItem.map((item) => {
          if (item.key === 3)
            return (
              <Row className="mt-5">
                <Col xl={5} lg={5}>
                  <img src={item.src} className="w-100 text-right" />
                </Col>
                <Col xl={7} lg={7} className="m-auto">
                  <h1 className="text-dark pb-3">{item.title}</h1>
                  <p className="text-secondary">{item.description}</p>
                </Col>
              </Row>
            );
        })}
      </Container>
    </div>
  );
}

export default About;
