import React from "react";
import { Container } from "react-bootstrap";
import img from "../Images/16.png";

function Team() {
  return (
    <div>
      <Container fluid className="px-0">
        <Container className="px-0 pt-5 pb-5 mt-4">
          <p className="text-info">// Our Team</p>
          <h1>
            <strong>Meet the Team of Innovators!</strong>
          </h1>
        </Container>
        <img src={img} className="w-100" />
      </Container>
    </div>
  );
}

export default Team;
