import React, { useState, useEffect } from "react";
import axios from "axios";
import { Container, Row, Col } from "react-bootstrap/esm";
import { fetchAllServices } from "./../api/index";
import { getAllServices } from "./../api/index";

function Services() {
  const [cardItems, setCardItems] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    // setLoading(true);
    // fetchAllServices().then((res) => {
    //   setCardItems(res);
    //   setLoading(false);
    // });
    response();
  }, []);

  console.log(loading);

  async function response() {
    try {
      setLoading(true);
      const res = await getAllServices();
      setCardItems(res);
      setLoading(false);
    } catch (error) {
      console.log(error);
    }
  }

  return (
    <div>
      <Container fluid>
        <Container className="px-0 pt-5 pb-5 mt-4">
          <p className="text-info">// Our Services</p>

          <h1>
            <strong>We Offer a Wide Variety of IT Services </strong>
          </h1>
          {cardItems.map((item) => {
            return (
              <Row className="mt-5 border" key={item.id}>
                <Col xl={3} lg={4} md={6}>
                  <img
                    src={`https://file.wastecambodia.com/uploads/${item.photo}`}
                    className="w-100"
                  />
                </Col>
                <Col xl={8} lg={7} md={6} className="my-auto">
                  <h3 className="mt-3">{item.title}</h3>
                  <p className="pt-3 text-secondary">{item.description}</p>
                </Col>
              </Row>
            );
          })}
        </Container>
      </Container>
    </div>
  );
}

export default Services;
