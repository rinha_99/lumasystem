import React, { useState, useEffect } from "react";
import { Row, Col, Card } from "react-bootstrap/esm/";
import { Link } from "react-router-dom";
import { BsArrowRight } from "react-icons/bs";
import { getFrontServices } from "../../api";

function CardService(props) {
  const { pathLink } = props;
  const [servicesCard, setServicesCard] = useState([]);

  useEffect(() => {
    response();
  }, []);

  async function response() {
    try {
      const res = await getFrontServices();
      setServicesCard(res);
    } catch (error) {
      console.log(error);
    }
  }

  return (
    <div>
      <Row className="gx-4">
        {servicesCard.map((item, index) => {
          return (
            <Col
              xl={4}
              lg={4}
              md={4}
              sm={12}
              xs={12}
              key={index}
              className="pb-3 overflow-hidden zoom"
            >
              <Card>
                <Card.Img
                  variant="top"
                  src={`https://file.wastecambodia.com/uploads/${item.photo}`}
                />
                <Card.Body>
                  <Card.Title>{item.title}</Card.Title>
                  <Card.Text className="text-secondary">
                    {item.description}
                  </Card.Text>
                  <Card.Link className="text-decoration-none">
                    <BsArrowRight /> &nbsp;&nbsp;
                    <Link to={`/${pathLink}`}>Learn More</Link>
                  </Card.Link>
                </Card.Body>
              </Card>
            </Col>
          );
        })}
      </Row>
    </div>
  );
}

export default CardService;
