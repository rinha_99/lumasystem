import React from "react";

import { Container, Row, Col } from "react-bootstrap/esm/";
import CardService from "./CardService";
import AboutUs from "./AboutUs";
import OurteamCard from "./OurteamCard";
import SlideCarousel from "./SlideCarousel";
import { contactItem } from "./../../constants/contact";
import { menuLink } from "./../../constants/index";

function Home() {
  const pathLink = menuLink.map((item) => {
    return item.path;
  });

  return (
    <div style={{ overflow: "hidden" }}>
      <SlideCarousel />

      <Container
        fluid
        className="bg-light pt-4 pb-4 mb-5 text-secondary contact-box"
      >
        <Container className="px-0">
          <Row>
            {contactItem.map((item, index) => {
              if (item.key === 4) return;
              return (
                <Col xl={4} lg={4} md={4} sm={12} key={index}>
                  <span className="text-info fs-1">{item.icon}</span>
                  <strong>&nbsp;&nbsp;{item.value}</strong>
                </Col>
              );
            })}
          </Row>
        </Container>
      </Container>

      <Container fluid className="text-center pt-5 pb-5 mb-5">
        <Container className="px-0">
          <h1 className="pb-5">All Our Services</h1>
          <div>
            <CardService pathLink={pathLink[2]} />
          </div>
        </Container>
      </Container>

      <Container
        fluid
        className="pt-5 pb-1 mb-5"
        style={{ background: "#08B8F8" }}
      >
        <AboutUs pathLink={pathLink[1]} />
      </Container>

      <Container fluid className="pt-5 pb-5 text-center team-box bg-light">
        <Container className="px-0">
          <h1>Our Team</h1>
          <p className="text-secondary pb-4">
            We help businesses elevate their value through custom software
            development, product design, QA and consultancy services.
          </p>

          <OurteamCard />
        </Container>
      </Container>
    </div>
  );
}

export default Home;
