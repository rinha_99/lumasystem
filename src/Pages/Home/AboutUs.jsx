import React from "react";
import { Row, Col, Container } from "react-bootstrap/esm/";
import { Link } from "react-router-dom";
import { BsArrowRight } from "react-icons/bs";
import { aboutusItem } from "./../../constants/aboutus";

function AboutUs(props) {
  return (
    <div>
      <Container className="mb-5 px-0">
        {aboutusItem.map((item, index) => {
          if (item.key === 1)
            return (
              <Row key={index}>
                <Col xl={7} lg={7} className="m-auto">
                  <img src={item.src} className="w-100" />
                </Col>
                <Col>
                  <h1 className=" text-center text-light">{item.title}</h1>
                  <p className="text-light text-center">{item.description}</p>
                  <Link
                    to={`/${props.pathLink}`}
                    className="text-decoration-none social_link"
                  >
                    <BsArrowRight />
                    &nbsp;&nbsp; Learn More About Us
                  </Link>
                </Col>
              </Row>
            );
        })}
      </Container>
    </div>
  );
}

export default AboutUs;
