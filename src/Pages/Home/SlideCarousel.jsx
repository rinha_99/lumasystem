import React from "react";
import { Carousel } from "react-bootstrap";
import { carouselItems } from "./../../constants/carousel";

function SlideCarousel() {
  return (
    <div>
      {/* {carouselItems.map((item) => {
        return (
          <div>
            <img src={item.src} />
            <p>{item.title}</p>
            <p>{item.description}</p>
          </div>
        );
      })} */}

      <div className="carousel">
        <Carousel fade>
          {carouselItems.map((item, index) => {
            return (
              <Carousel.Item key={index}>
                <div className="bg"></div>
                <img className=" w-100 " src={item.src} />

                <Carousel.Caption>
                  <h1 className="pb-2">{item.title}</h1>
                  <p>{item.description}</p>
                </Carousel.Caption>
              </Carousel.Item>
            );
          })}
        </Carousel>
      </div>
    </div>
  );
}

export default SlideCarousel;
