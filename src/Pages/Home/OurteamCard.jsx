import React from "react";
import { Row, Col, Card } from "react-bootstrap/esm";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { ourteamItem } from "./../../constants/ourteam";

function OurteamCard() {
  var setting = {
    dots: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    mobileFirst: true,
    autoplay: true,
    autoplaySpeed: 3000,
    responsive: [
      {
        breakpoint: 991.98,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          dots: true,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          dots: true,
        },
      },
    ],
  };

  return (
    <div>
      <Row>
        <Slider {...setting}>
          {ourteamItem.map((item, index) => {
            return (
              <Col key={index}>
                <Card>
                  <Card.Img variant="top" src={item.src} />
                </Card>
              </Col>
            );
          })}
        </Slider>
      </Row>
    </div>
  );
}

export default OurteamCard;
