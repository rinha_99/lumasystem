import React from "react";
import { Container, Row, Col } from "react-bootstrap/esm";
import { GiGearHammer } from "react-icons/gi";
import { GiAchievement } from "react-icons/gi";
import man from "../Images/man1.png";

function Partner() {
  return (
    <div>
      <Container className="pt-5 mt-4 gx-5">
        <Row>
          <Col xl={4} lg={5} md={6}>
            <img src={man} className="w-100" />
          </Col>
          <Col xl={6} lg={7} md={6}>
            <p className="text-info">// Why Choose us</p>
            <h1>
              <strong>Your Partner for Software Innovation</strong>
            </h1>
            <p className="text-secondary">
              Engitech is the partner of choice for many of the leading
              enterprises, SMEs and technology challengers. We help businesses
              elevate their value through custom software development, product
              design, QA and consultancy services.
            </p>
            <div className="d-flex pt-5">
              <div className="d-flex">
                <GiAchievement className="text-info" style={{ fontSize: 56 }} />
                &nbsp;&nbsp;
                <h4>Experiences</h4>
              </div>
              <div className="d-flex px-5">
                <GiGearHammer className="fs-1 text-info" />
                &nbsp;&nbsp;
                <h4>Quick Support</h4>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default Partner;
