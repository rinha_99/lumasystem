import { React, useState, useRef } from "react";
import emailjs from "@emailjs/browser";
import { contactItem } from "../constants/contact";
import { AddressMap } from "./../Component/AddressMap";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  InputGroup,
} from "react-bootstrap/esm";

function Contact() {
  //send message
  const form = useRef();
  const [validated, setValidated] = useState(false);
  const [inputName, setInputName] = useState();
  const [inputEmail, setInputEmail] = useState();
  const [inputMessage, setInputMessage] = useState();

  const handleName = (e) => {
    setInputName(e.target.value);
  };
  const handleEmail = (e) => {
    setInputEmail(e.target.value);
  };
  const handleMessage = (e) => {
    setInputMessage(e.target.value);
  };

  const handleSubmit = (event) => {
    const formInput = event.currentTarget;

    if (formInput.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    } else {
      emailjs
        .sendForm(
          "service_9dquf4f",
          "template_c1zmsd4",
          form.current,
          "AQXyi13iA1t187NaQ"
        )
        .then(
          (result) => {
            console.log(result.text);
            alert("Message has been sent!");
          },
          (error) => {
            console.log(error.text);
          }
        );
      event.preventDefault();
      setInputName("");
      setInputEmail("");
      setInputMessage("");
    }
    setValidated(true);
  };

  return (
    <div>
      <Container className="pt-5 pb-5 mt-4">
        <Row className="">
          <Col xl={6} lg={6}>
            <p className="text-info">// Contact Detail</p>
            <h1>
              <strong>Contact Us</strong>
            </h1>
            <p className="text-secondary mb-5">
              Give us a call or drop by anytime, we endeavour to answer all
              enquiries within 24 hours on business days. We will be happy to
              answer your questions.
            </p>
            {contactItem.map((item) => {
              if (item.key === 4) return;
              return (
                <div className="d-flex mb-3">
                  <span className="text-info fs-1">{item.icon}</span>
                  <span className="px-3 pb-2">
                    <strong>{item.label}</strong>
                    <p className="text-secondary mt-2">{item.value}</p>
                  </span>
                </div>
              );
            })}
          </Col>

          <Col xl={6} lg={6}>
            <Form
              className="bg-info p-5"
              noValidate
              validated={validated}
              ref={form}
              onSubmit={handleSubmit}
            >
              <h3>
                <strong>Ready to Get Started?</strong>
              </h3>
              <p className="fst-italic text-secondary pt-3">
                Your email address will not be published. Required fields are
                marked <span className="text-danger">*</span>
              </p>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Label>
                  Your Name <span className="text-danger">*</span>
                </Form.Label>
                <InputGroup hasValidation>
                  <Form.Control
                    type="text"
                    name="user_name"
                    onChange={handleName}
                    required
                    value={inputName}
                  />
                  <Form.Control.Feedback type="invalid">
                    Please provide your name.
                  </Form.Control.Feedback>
                </InputGroup>
              </Form.Group>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Label>
                  Your Email <span className="text-danger">*</span>
                </Form.Label>
                <InputGroup hasValidation>
                  <Form.Control
                    type="text"
                    name="user_email"
                    onChange={handleEmail}
                    required
                    value={inputEmail}
                  />
                  <Form.Control.Feedback type="invalid">
                    Please provide your email address.
                  </Form.Control.Feedback>
                </InputGroup>
              </Form.Group>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlTextarea1"
              >
                <Form.Label>Message</Form.Label>
                <Form.Control
                  as="textarea"
                  name="message"
                  rows={3}
                  onChange={handleMessage}
                  value={inputMessage}
                />
              </Form.Group>

              <Button type="submit">Send Message</Button>
            </Form>
          </Col>
        </Row>
      </Container>
      <AddressMap />
    </div>
  );
}

export default Contact;
