import { useLocation } from "react-router-dom";
import { replaceString } from "./../Utils/helpers";

const useGetPathName = () => {
  const location = useLocation().pathname;
  const pathName = replaceString(location);
  return pathName;
};

export default useGetPathName;
